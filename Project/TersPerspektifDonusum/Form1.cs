﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 *	Emre Özel
 *	emreozel.net
 * 
 */
namespace TersPerspektifDonusum
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private Bitmap pattern;
        private double[,] view;
        private double beta = 0, alfa = 0, teta = 0, D = 10; // Degree values

        private double[,] MatrixInverse3x3(double[,] m) // bu fonksiyon yerine ters matrisler de initialize edilebilir.
        {
            double determinant = m[0, 0] * (m[1, 1] * m[2, 2] - m[2, 1] * m[1, 2]) -
             m[0, 1] * (m[1, 0] * m[2, 2] - m[1, 2] * m[2, 0]) +
             m[0, 2] * (m[1, 0] * m[2, 1] - m[1, 1] * m[2, 0]);

            double invDet = 1 / determinant;

            double[,] result = new double[3, 3];
            result[0, 0] = (m[1, 1] * m[2, 2] - m[2, 1] * m[1, 2]) * invDet;
            result[0, 1] = (m[0, 2] * m[2, 1] - m[0, 1] * m[2, 2]) * invDet;
            result[0, 2] = (m[0, 1] * m[1, 2] - m[0, 2] * m[1, 1]) * invDet;
            result[1, 0] = (m[1, 2] * m[2, 0] - m[1, 0] * m[2, 2]) * invDet;
            result[1, 1] = (m[0, 0] * m[2, 2] - m[0, 2] * m[2, 0]) * invDet;
            result[1, 2] = (m[1, 0] * m[0, 2] - m[0, 0] * m[1, 2]) * invDet;
            result[2, 0] = (m[1, 0] * m[2, 1] - m[2, 0] * m[1, 1]) * invDet;
            result[2, 1] = (m[2, 0] * m[0, 1] - m[0, 0] * m[2, 1]) * invDet;
            result[2, 2] = (m[0, 0] * m[1, 1] - m[1, 0] * m[0, 1]) * invDet;
            return result;
        }

        private double[,] MatrixMultiplication(double[,] m1, double[,] m2)
        {
            double[,] result = new double[m1.GetLength(0), m2.GetLength(1)];
            /*
             *    Row size: GetLength(0)
             * Column size: GetLength(1);
             */
            if (m1.GetLength(1) == m2.GetLength(0))
            {
                for(int i = 0; i < m1.GetLength(0); i++)
                {
                    for (int j = 0; j < m2.GetLength(1); j++)
                    {
                        for (int k = 0; k < m2.GetLength(0); k++)
                        {
                            result[i, j] = m1[i, k] * m2[k, j] + result[i, j];
                        }
                    }
                }
            }
            return result;
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox1.Text) && String.IsNullOrEmpty(textBox2.Text) && String.IsNullOrEmpty(textBox3.Text))
            {
                // If all textboxes is empty do something
                DialogResult msg = MessageBox.Show("Lütfen tüm alanları doldurun.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (pattern == null)
            {
                DialogResult msg = MessageBox.Show("Lütfen önce görseli seçin.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                // Create 3x1 field of view matrix
                view = new double[3, 1];

                // Field of view values
                try
                {
                    view[0, 0] = Double.Parse(textBox1.Text, CultureInfo.InvariantCulture); // X(A)
                    view[1, 0] = Double.Parse(textBox2.Text, CultureInfo.InvariantCulture); // Y(A)
                    view[2, 0] = Double.Parse(textBox3.Text, CultureInfo.InvariantCulture); // Z(A)
                    D = Double.Parse(textBox4.Text, CultureInfo.InvariantCulture); // D
                }
                catch (Exception ex)
                {
                    // If textbox value is "."
                    DialogResult msg = MessageBox.Show("Lütfen geçerli değerler girin.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                try
                {
                    beta = Int16.Parse(textBox5.Text);
                    alfa = Int16.Parse(textBox6.Text);
                    teta = Int16.Parse(textBox7.Text);
                }
                catch (Exception ex)
                {
                    // If degree values is empty
                    textBox5.Text = beta.ToString();
                    textBox6.Text = alfa.ToString();
                    textBox7.Text = teta.ToString();
                }

                // Create 3x3 rotation matrix
                double[,] r1 = new double[3, 3];
                double[,] r2 = new double[3, 3];
                double[,] r3 = new double[3, 3];

                // Rotation matrix values
                r1[0, 0] = Math.Cos(beta);
                r1[0, 1] = 0;
                r1[0, 2] = -Math.Sin(beta);
                r1[1, 0] = 0;
                r1[1, 1] = 1;
                r1[1, 2] = 0;
                r1[2, 0] = Math.Sin(beta);
                r1[2, 1] = 0;
                r1[2, 2] = Math.Cos(beta);

                r2[0, 0] = 1;
                r2[0, 1] = 0;
                r2[0, 2] = 0;
                r2[1, 0] = 0;
                r2[1, 1] = Math.Cos(alfa);
                r2[1, 2] = Math.Sin(alfa);
                r2[2, 0] = 0;
                r2[2, 1] = -Math.Sin(alfa);
                r2[2, 2] = Math.Cos(alfa);

                r3[0, 0] = Math.Cos(teta);
                r3[0, 1] = Math.Sin(teta);
                r3[0, 2] = 0;
                r3[1, 0] = -Math.Sin(teta);
                r3[1, 1] = Math.Cos(teta);
                r3[1, 2] = 0;
                r3[2, 0] = 0;
                r3[2, 1] = 0;
                r3[2, 2] = 1;

                double[,] RMatrix = MatrixMultiplication(MatrixMultiplication(MatrixInverse3x3(r1), MatrixInverse3x3(r2)), MatrixInverse3x3(r3));
                double K, denominator;
                double[,] xyzMatrix = new double[3, 1];
                double[,] last;

                Bitmap rendered = new Bitmap(pattern.Width, pattern.Height);
                for (int XS = 0; XS < rendered.Width; XS++)
                {
                    for (int ZS = 0; ZS < rendered.Height; ZS++)
                    {
                        last = new double[3, 1];
                        last[0, 0] = XS - (rendered.Width / 2);
                        last[1, 0] = D;
                        last[2, 0] = -ZS + (rendered.Height / 2);

                        denominator = (RMatrix[2, 0] * last[0, 0] + RMatrix[2, 1] * last[1, 0] + RMatrix[2, 2] * last[2, 0]);
                        if (denominator < 0)
                        {
                            K = -view[2, 0] / denominator;

                            xyzMatrix = MatrixMultiplication(RMatrix, last);
                            xyzMatrix[0, 0] = view[0, 0] + K * xyzMatrix[0, 0];
                            xyzMatrix[1, 0] = view[1, 0] + K * xyzMatrix[1, 0];
                            xyzMatrix[2, 0] = view[2, 0] + K * xyzMatrix[2, 0];

                            rendered.SetPixel(XS, ZS, pattern.GetPixel(Convert.ToInt32(Math.Abs(xyzMatrix[0, 0])) % pattern.Width, Convert.ToInt32(Math.Abs(xyzMatrix[1, 0])) % pattern.Height));
                        }
                    }
                    imageRendered.Image = rendered;
                }
            }
        }

        private void OnlyDouble_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 46 && e.KeyChar != 8)
                e.Handled = true;
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            imageFile.Filter = "JPG, JPEG, PNG, BMP Files|*.jpg;*.jpeg;*.png;*.bmp";
            imageFile.Title = "Görseli Seç";
            
            if (imageFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                pattern = new Bitmap(imageFile.FileName);
                imagePattern.Image = pattern;
            }
        }
    }
}
