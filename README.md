Ters Perspektif Dönüşümü ile Doku Kaplama (Texture Mapping with Reverse Perspective Transformation)
======================================

Seçilen görselin girilen bakış noktası ve açılar yardımıyla ters perspektif dönüşümü hesaplanarak doku kaplama gerçekleştirilir.

![Ters Perspektif Dönüşümü ile Doku Kaplama Ekran Görüntüsü](https://bitbucket.org/emreozel/ters-perspektif-donusumu-ile-doku-kaplama/raw/HEAD/screenshot.png)

İndirme
-----------------

```shell
git clone git@bitbucket.org:emreozel/ters-perspektif-donusumu-ile-doku-kaplama.git
```


Notlar
-----

Matrisler ve diğer detaylar eklenecek